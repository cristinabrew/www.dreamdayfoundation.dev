<?php 
/**
 * The template for displaying all single posts and attachments
 */

get_header(); ?>

<div class="page-container">		

		<?php if ( !is_front_page() ) {?> <!-- Display only if not home -->															
			<?php get_template_part( 'parts/content', 'post-header'); ?>
		<? }?>
			
		<div class="post-content grid-container">

			<div class="inner-content grid-x grid-margin-x grid-padding-x">

				<main class="main small-12 medium-12 large-12 cell" role="main">
				
				    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
				
				    	<?php get_template_part( 'parts/loop', 'single' ); ?>
				    	
				    <?php endwhile; else : ?>
				
				   		<?php get_template_part( 'parts/content', 'missing' ); ?>

				    <?php endif; ?>

				</main> <!-- end #main -->

			</div> <!-- end #inner-content -->

		</div> <!-- end #content -->
	</div> <!-- end .page-container -->
<?php get_footer(); ?>