<?php
/*
Template Name: Frontpage
*/

get_header(); ?>

	<div class="fullscreen-image-slider">
	  <div class="orbit" role="region" aria-label="Dream Day Foundation" data-orbit>
	    <ul class="orbit-container">
	      <button class="orbit-previous">
	        <span class="show-for-sr">Previous Slide</span>
	        <span class="nav fa fa-chevron-left fa-3x"></span>
	      </button>
	      <button class="orbit-next">
	        <span class="show-for-sr">Next Slide</span>
	        <span class="nav fa fa-chevron-right fa-3x"></span>
	      </button>
	      <li class="is-active orbit-slide">
	        <img class="orbit-image" src="<?php echo get_theme_file_uri( '/assets/images/DDF_homepage-slider@2x_1.jpg' ); ?>" alt="Space">
	        <figcaption class="orbit-caption">
	          <h1>The best thing about<br/><span>Memories</span></h1>
	      	  <h4>is getting to make them.</h4>
	        </figcaption>
	      </li>
	      <li class="orbit-slide">
	        <img class="orbit-image" src="<?php echo get_theme_file_uri( '/assets/images/DDF_homepage-slider@2x_2.jpg' ); ?>" alt="Space">
	        <figcaption class="orbit-caption">
	          <h1>It's all about<br/><span>The Kids!</span><br/></h1>
	          <h4>Keep the spirit and promise of joy in their hearts.</h4>
	        </figcaption>
	      </li>
	      <li class="orbit-slide">
	        <img class="orbit-image" src="<?php echo get_theme_file_uri( '/assets/images/DDF_homepage-slider_3@2x.jpg' ); ?>" alt="Space">
	        <figcaption class="orbit-caption">
	          <h1>Benefitting the<br/><span>Patients</span></h1>
	          <h4>and families of St. Jude.</h4>
	        </figcaption>
	      </li>
	    </ul>
	  </div>
	</div>

	<div class="grid-container">
		<div class="dd-mission inner-content grid-x grid-margin-x grid-padding-x">
			<div class="small-12 medium-12 large-4 cell text-center">
				<img src="<?php echo get_theme_file_uri( '/assets/images/ddf-fish.png' ); ?>">
				<h4>Dream Day Mission</h4>
				<p>The Dream Day Foundation purpose and mission is to support the life-saving efforts of St. Jude Children's Research Hospital through fundraising, advocacy, and special events in order to enhance the quality of life for patients and their families.</p>
			</div>
			<div class="small-12 medium-12 large-4 cell text-center">
				<img src="<?php echo get_theme_file_uri( '/assets/images/ddf-stjude.png' ); ?>">
				<h4>St. Jude Mission</h4>
				<p>The mission of St. Jude Children's Research Hospital is to advance cures, and means of prevention, for pediatric catastrophic diseases through research and treatment. Consistent with the vision of founder Danny Thomas, no child is denied treatment based on race, religion or a family's ability to pay.</p>
			</div>
			<div class="small-12 medium-12 large-4 cell text-center">
				<img src="<?php echo get_theme_file_uri( '/assets/images/ddf-heart.png' ); ?>">
				<h4>Volunteer</h4>
				<p>Through fishing trips, cookoffs, musical events, and more, the Dream Day Foundation has found their way into many hearts of the St. Jude children and their families. This organization could not function without the support of the many volunteers. Thank you to all who give their time!</p>
			</div>
		</div>
	</div>
	
	<div class="home-content">	
		<div class="content grid-container">
		
			<div class="inner-content grid-x grid-margin-x grid-padding-x">
		
			    <main class="main small-12 medium-12 large-12 cell" role="main">
					
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

						<?php get_template_part( 'parts/loop', 'page' ); ?>
						
					<?php endwhile; endif; ?>							

				</main> <!-- end #main -->
			    
			</div> <!-- end #inner-content -->
		
		</div> <!-- end #content -->
	</div>

<?php get_footer(); ?>
