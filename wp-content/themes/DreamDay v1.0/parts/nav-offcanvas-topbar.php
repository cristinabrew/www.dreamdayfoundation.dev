<?php
/**
 * The off-canvas menu uses the Off-Canvas Component
 *
 * For more info: http://jointswp.com/docs/off-canvas-menu/
 */
?>
<div data-sticky-container>
	<div class="top-bar-wrapper" data-sticky data-options="marginTop:0;" style="width:100%">
		<div class="top-bar show-for-medium" id="pre-top-bar">

			<?php get_sidebar('announcement'); ?>

			<div class="top-bar-right float-right">
				<a class="button primary" href="http://eepurl.com/bZkd1b" target="_blank">Signup for our Newsletter</a><a class="button success" href="/donate/">Donate Now</a>
			</div>
		</div>

		<div class="top-bar" id="top-bar-menu">
			<div class="top-bar-left float-left">
				<ul class="menu">
					<li><a href="<?php echo home_url(); ?>"><img src="<?php echo get_theme_file_uri( '/assets/images/DDF-Logo-New.png' ); ?>" class="logo"></a></li>
				</ul>
			</div>
			<div class="top-bar-right show-for-medium">
				<?php joints_top_nav(); ?>	
			</div>
			<div class="top-bar-right float-right show-for-small-only">
				<ul class="menu">
					<li><button class="menu-icon" type="button" data-toggle="off-canvas"></button></li>
				</ul>
			</div>
		</div>
	</div>
</div>