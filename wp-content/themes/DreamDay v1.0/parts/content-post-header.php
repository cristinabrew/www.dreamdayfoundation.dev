					
	<header class="page-header text-center">
		<hr class="yellow-hr-sm">
		<div class="grid-container">
			<div class="grid-x grid-margin-x grid-padding-x">
				<div class="small-12 medium-12 large-12 cell">
					<h1 class="post-title"><?php the_title(); ?></h1>
					<?php get_template_part( 'parts/content', 'byline' ); ?>
				</div>
			</div>
		</div>
	</header> <!-- end article header -->
