<?php 
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 */

get_header(); ?>
	
	<div class="page-container">

		<?php if ( !is_front_page() ) {?> <!-- Display only if not home -->							
			<?php get_template_part( 'parts/content', 'page-header'); ?>
		<? }?>
	
		<div class="page-content grid-container">
			<div class="inner-content grid-x grid-margin-x grid-padding-x">
		
			    <main class="main small-12 medium-12 large-8 cell" role="main">
					
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

						<?php get_template_part( 'parts/loop', 'page' ); ?>
						
					<?php endwhile; endif; ?>							

				</main>

				 <?php get_sidebar(); ?>
			    
			</div> 
		</div>
	
	</div> <!-- end .page-container -->

<?php get_footer(); ?>


