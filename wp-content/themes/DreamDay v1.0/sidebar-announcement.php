<?php 
/**
 * The sidebar containing the announcement widget area
 */
 ?>



	<?php if ( is_active_sidebar( 'announcement' ) ) : ?>

		<?php dynamic_sidebar( 'announcement' ); ?>

	<?php else : ?>

	<!-- This content shows up if there are no widgets defined in the backend. -->
		<div class="top-bar-left float-left">
			<span></span>
		</div>		
	

	<?php endif; ?>

