<?php
/**
 * The template for displaying the footer. 
 *
 * Comtains closing divs for header.php.
 *
 * For more info: https://developer.wordpress.org/themes/basics/template-files/#template-partials
 */			
 ?>
					
				<footer class="footer" role="contentinfo">
					
					<div class="grid-container">
						<div class="inner-footer grid-x grid-margin-x grid-padding-x">
							
							<div class="small-12 medium-4 large-4 cell">
	<!-- 							<nav role="navigation">
		    						<?php joints_footer_links(); ?>
		    					</nav> -->
		    					<h5>Dream Day Mission</h5>
								<p>The Dream Day Foundation purpose and mission is to support the life-saving efforts of St. Jude Children's Research Hospital through fundraising, advocacy, and special events in order to enhance the quality of life for patients and their families.</p>
		    				</div>

		    				<div class="small-12 medium-4 large-4 cell">
		    				</div>

		    				<div class="small-12 medium-4 large-4 cell">
		    					<h5>Contact Us</h5>
		    					<p>11919 Bricksome Ave., Suites E-6 &amp; E-8<br/>
								Baton Rouge, LA 70816<br/></p>
								<h6><a href="https://goo.gl/maps/wYVJocAgSgq" alt="Google Map" target="_blank">Map it <i class="fa fa-chevron-right"></i></a></h6>
								<p><i class="fas fa-phone"></i><a href="tel:12257545700">225-754-5700</a><br/>
								<i class="fas fa-envelope"></i><a href="mailto:info@dreamdayfoundation.org">info@dreamdayfoundation.org</a></p>

								<div class="social-links">
									<a href="https://www.facebook.com/dreamdayfoundation" alt="Facebook" target="_blank"><i class="fab fa-facebook-square"></i></a>
									<a href="https://www.twitter.com/dreamdayfdn" alt="Twitter" target="_blank"><i class="fab fa-twitter"></i></a>
									<a href="https://www.youtube.com/user/DreamDayFoundation1" alt="YouTube" target="_blank"><i class="fab fa-youtube"></i></a>
								</div>
		    				</div>
						
						</div> <!-- end #inner-footer -->
					</div>

					<div class="grid-container">
						<div class="bottom-footer grid-x grid-margin-x grid-padding-x">
							<div class="small-12 medium-12 large-12 cell">
								<p class="source-org copyright"><small>&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?> | Website Designed and Donated by <a href="https://think-brew.com" alt="Brew Agency Baton Rouge" target="_blank">Brew Agency.</small></a></p>
							</div>
						</div>
					</div>
				
				</footer> <!-- end .footer -->
			
			</div>  <!-- end .off-canvas-content -->
					
		</div> <!-- end .off-canvas-wrapper -->
		
		<?php wp_footer(); ?>
		
	</body>
	
</html> <!-- end page -->