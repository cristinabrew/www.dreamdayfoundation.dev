<?php
/*
Template Name: Full Width (No Sidebar)
*/

get_header(); ?>
			
	<div class="page-container">

		<?php if ( !is_front_page() ) {?> <!-- Display only if not home -->							
			<?php get_template_part( 'parts/content', 'page-header'); ?>
		<? }?>
	
		<div class="fw-page-content grid-container">
			<div class="inner-content grid-x grid-margin-x grid-padding-x">
		
			    <main class="main small-12 medium-12 large-12 cell" role="main">
				
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

						<?php get_template_part( 'parts/loop', 'page' ); ?>
						
					<?php endwhile; endif; ?>	

				</main>						

			</div> <!-- end .inner-content -->
		    
		</div> <!-- end .content -->

		<div class="pre-footer-cta">
			<div class="grid-container">
				<div class="grid-x grid-margin-x">
					<div class="small-12 medium-6 large-6 cell">
						<h2>Every gift is a gift of <em>life</em></h2>
						<p>We invited you to help us continue on our mission to support St. Jude Children's Research Hospital. No gift is too small or too large when it comes to our children and their future.</p>					
					</div>
					<div class="small-12 medium-6 large-6 cell text-center">
						<a href="/donate/" class="button primary large">Make a Donation</a>
					</div>
				</div>
			</div>
		</div>
	
	</div> <!-- end .page-container -->

<?php get_footer(); ?>
