<?php
/*
About Us Page Template
*/

get_header(); ?>
			
	<div class="page-container">

		<?php if ( !is_front_page() ) {?> <!-- Display only if not home -->							
			<?php get_template_part( 'parts/content', 'page-header'); ?>
		<? }?>
	
		<div class="fw-page-content grid-container">
			<div class="inner-content grid-x grid-margin-x grid-padding-x">
		
			    <main class="main small-12 medium-12 large-12 cell" role="main">
				
					<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

						<?php get_template_part( 'parts/loop', 'page' ); ?>
						
					<?php endwhile; endif; ?>	

				</main>		

				<section class="team small-12 medium-12 large-12 cell">
					<?php if( have_rows('team_member')) : ?>
					<h3 class="text-center">Our Staff</h3>
					<div class="grid-x grid-margin-x">
						<?php while( have_rows('team_member')) : the_row(); ?>
						<div class="small-auto large-auto cell text-center">
							<h4><?php the_sub_field('name'); ?></h4>
							<em><?php the_sub_field('title'); ?></em>
						</div>
						<?php endwhile; ?>
					</div>
					<?php endif; ?>
				</section>

				<section class="leadership small-12 medium-12 large-12 cell">
					<h3 class="text-center">Leadership</h3>
					<div class="grid-x grid-margin-x grid-padding-x">
						<div class="small-12 medium-6 large-6 cell text-center">
							<h5>Board of Governors</h5>
							<?php the_field('board_governors'); ?>
						</div>
						<div class="small-12 medium-6 large-6 cell text-center">
							<h5>Founding Board Members</h5>
							<?php the_field('founding_board'); ?>
						</div>
					</div>
					<div class="advisory-board grid-x grid-margin-x grid-padding-x">
						<div class="small-12 medium-12 large-12 cell text-center">
							<h5>Advisory Board Members</h5>
							<?php the_field('advisory_board'); ?>
						</div>
					</div>
				</section>				

			</div> <!-- end .inner-content -->
		    
		</div> <!-- end .content -->
	
	</div> <!-- end .page-container -->

<?php get_footer(); ?>
