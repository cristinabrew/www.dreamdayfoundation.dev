<?php
/** 
 * For more info: https://developer.wordpress.org/themes/basics/theme-functions/
 *
 */			
	
// Theme support options
require_once(get_template_directory().'/functions/theme-support.php'); 

// WP Head and other cleanup functions
require_once(get_template_directory().'/functions/cleanup.php'); 

// Register scripts and stylesheets
require_once(get_template_directory().'/functions/enqueue-scripts.php'); 

// Register custom menus and menu walkers
require_once(get_template_directory().'/functions/menu.php'); 

// Register sidebars/widget areas
require_once(get_template_directory().'/functions/sidebar.php'); 

// Makes WordPress comments suck less
require_once(get_template_directory().'/functions/comments.php'); 

// Replace 'older/newer' post links with numbered navigation
require_once(get_template_directory().'/functions/page-navi.php'); 

// Adds support for multiple languages
require_once(get_template_directory().'/functions/translation/translation.php'); 

// Adds site styles to the WordPress editor
// require_once(get_template_directory().'/functions/editor-styles.php'); 

// Remove Emoji Support
// require_once(get_template_directory().'/functions/disable-emoji.php'); 

// Related post function - no need to rely on plugins
// require_once(get_template_directory().'/functions/related-posts.php'); 

// Use this as a template for custom post types
// require_once(get_template_directory().'/functions/custom-post-type.php');

// Customize the WordPress login menu
// require_once(get_template_directory().'/functions/login.php'); 

// Customize the WordPress admin
// require_once(get_template_directory().'/functions/admin.php'); 


// Gravity Forms - Remove Selections

add_action( 'plugins_loaded', 'jwsf_gform_filters' );
function jwsf_gform_filters() {
	if ( ! is_admin() )
		add_filter( 'gform_field_content', 'jwsf_form_choices', 1, 5 );
}

function jwsf_form_choices( $content, $field, $value, $lead_id, $form_id )
{

  /* Check to make sure we're loading the right form 
  Change the "2" to the ID of the form you're modifying
  */
  if ( 2 !== (int) $form_id ) return $content;

  /* Check to make sure this is the field we want to filter
  Change the number to the appropriate field ID
  */
  if ( 7 !== (int) $field['id'] ) return $content;

  /* Retrieve a list of the options that were already selected */
  global $wpdb;
  $tablename = $wpdb->prefix . 'rg_lead_detail';

  /* Change the "2" to the ID of your form
  Change the 7 to the ID of the field
  */
  $values = $wpdb->get_col( $wpdb->prepare( "SELECT value FROM $tablename WHERE form_id=%d AND field_number=%d ORDER BY lead_id", 2, 7 ) );

  /* Loop through all of the options available in this field
  If that choice has already been chosen by someone else, 
  add a "readonly" and "disabled" attribute to the input, 
  and make the label strikethrough
  */

  foreach ( $field['choices'] as $k => $f ) {
    if ( ! in_array( $f['value'], $values ) ) continue;

    $content = str_replace( array( "value='{$f['value']}'", "for='choice_{$field['id']}_$k'" ), array( "readonly disabled value='{$f['value']}'", "style='text-decoration: line-through' 'choice_{$field['id']}_$k'" ), $content );
  }

  /* Return the filtered content of the form field */
  return $content;
}